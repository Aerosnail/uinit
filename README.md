uInit, a simple initramfs generator
===================================

More of an exercise to understand how initramfs images work than a practical
substitute for `dracut` or `mkitincpio`, but useful in its own right when the
initramfs must be embedded in the kernel binary (e.g. to have a monolithic
binary that can be signed with secureboot keys).

**WARNING:** this script is extremely tuned to my own needs, and probably won't
work if you have a different setup (non-encrypted root partition, different
keyboard layout etc...)
